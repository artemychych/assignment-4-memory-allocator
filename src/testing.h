#ifndef TESTING_H
#define TESTING_H

#include <stdint.h>
#include <sys/mman.h>

#include "mem.h"
#include "mem_internals.h"
#include "util.h"

void run();

#endif