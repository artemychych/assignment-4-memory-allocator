#include "testing.h"

void* heap;
struct block_header* block;

static int assert_block_count(struct block_header* block) {
    int count = 0;
    struct block_header *iter = block;
    while (iter->next) {
        if (!iter->is_free) {
            count++;
        }
        iter = iter->next;
    }
    return count;
}

static bool init_testing_heap() {
    heap = heap_init(REGION_MIN_SIZE);
    block = (struct block_header *) heap;
    if (!heap || !block)
        return false;
    return true;
}

void separation() {
    printf("---------------------------\n");
}

static bool first_test() {
    printf("Test 1: \n");
    debug_heap(stdout, heap);
    printf("<-1024->\n");
    void *mmalloc = _malloc(1024);
    debug_heap(stdout, heap);
    if (!mmalloc || block->capacity.bytes != 1024) {
        _free(mmalloc);
        return false;
    } else {
        _free(mmalloc);
        return true;
    }
    
}

static bool second_test() {
    printf("Test 2:\n");
    debug_heap(stdout, heap);
    printf("<-1024->\n");
    void *mmalloc1 = _malloc(1024);
    printf("<-2048->\n");
    void *mmalloc2 = _malloc(2048);
    debug_heap(stdout, heap);
    if (assert_block_count(block) != 2) {
        _free(mmalloc1);
        _free(mmalloc2);
        return false;
    } else {
        _free(mmalloc1);
        debug_heap(stdout, heap);
        if (assert_block_count(block) == 1) {
            _free(mmalloc2);
            return true;
        } else {
            _free(mmalloc2);
            return false;
        }
    }
}

static bool third_test() {
    printf("Test 3:\n");
    debug_heap(stdout, heap);
    printf("<-1024->\n");
    void *mmalloc1 = _malloc(1024);
    printf("<-2048->\n");
    void *mmalloc2 = _malloc(2048);
    printf("<-4096->\n");
    void *mmalloc3 = _malloc(4096);
    debug_heap(stdout, heap);
    if (assert_block_count(block) != 3) {
        
        _free(mmalloc1);
        _free(mmalloc2);
        _free(mmalloc3);
        return false;
    } else {
        _free(mmalloc1);
        _free(mmalloc2);
        debug_heap(stdout, heap);
        if (assert_block_count(block) == 1) {
            _free(mmalloc3);
            return true;
        }else {
            _free(mmalloc3);
            return false;
        }
    }
}

void run() {
    if (init_testing_heap()) {
        int flag = 0;
        printf("Initializing tests: ...");
        separation();
        if (first_test()) {
            flag++;
            printf("TEST 1 PASSED\n");
        } else {
            printf("TEST 1 FAILED\n");
        }
        separation();
        
        
        if (second_test()) {
            flag++;
            printf("TEST 2 PASSED\n");
        } else {
            printf("TEST 2 FAILED\n");
        }
        separation();
        if (third_test()) {
            flag++;
            printf("TEST 3 PASSED\n");
        } else {
            printf("TEST 3 FAILED\n");
        }
        printf("%d of 3 TESTS PASSED\n", flag);
    } else {
        printf("Error initializing heap\n");
    }
}